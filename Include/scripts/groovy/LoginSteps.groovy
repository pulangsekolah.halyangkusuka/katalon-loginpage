import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class LoginSteps {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@Given("User is on the landing page")
	def navigateToLoginPage() {
		Mobile.startApplication('C:\\Users\\Samuel Pangestu\\IdeaProjects\\App\\com.bareksa.app_2023-07-25.apk', true)
		Mobile.verifyElementExist(findTestObject('Object Repository/LoginPage/android.view.ViewGroup'), 0)
	}

	@When("User clicks the login button on the landing page")
	def clickLandingPageLoginButton() {
		Mobile.tap(findTestObject('Object Repository/LoginPage/android.widget.Button - Login'), 0)
	}

	@And("User inputs valid (.*) and (.*)")
	def inputEmailPassword(String username, String password) {
		Mobile.setText(findTestObject('Object Repository/LoginPage/android.widget.EditText - Contoh emailgmail.com'), username, 0)
		Mobile.setText(findTestObject('Object Repository/LoginPage/android.widget.EditText - Masukkan Password'), password, 0)
	}

	@And("User inputs only valid (.*)")
	def inputPassword(String password) {
		Mobile.setText(findTestObject('Object Repository/LoginPage/android.widget.EditText - Masukkan Password'), password, 0)
	}

	@When("User clicks the login button on the login page")
	def clickLoginPageLoginButton() {
		Mobile.tap(findTestObject('Object Repository/LoginPage/android.widget.Button - Login (1)'), 0)
	}

	@Then("User should be successfully logged in")
	def verifyOTP() {
		Mobile.getText(findTestObject('Object Repository/LoginPage/android.widget.TextView - Kode OTP telah dikirim melalui email ke samhotmail.com'),
				0)
		Mobile.closeApplication()
	}

	@Then("Message 'Email atau password salah. Silakan coba lagi' will appear")
	def verifyWrongCredential() {
		Mobile.getText(findTestObject('Object Repository/LoginPage/android.widget.TextView - Email atau password salah. Silakan coba lagi'), 0)
		Mobile.closeApplication()
	}

	@Then("Message 'Format email salah.' will appear")
	def verifyWrongEmailFormat() {
		Mobile.getText(findTestObject('Object Repository/LoginPage/android.widget.TextView - Format email salah'), 0)
		Mobile.closeApplication()
	}

	@Then("Message 'Wajib diisi.' will appear")
	def verifyBlankUsernameAndPassword() {
		Mobile.getText(findTestObject('Object Repository/LoginPage/android.widget.TextView - Wajib diisi'), 0)
		Mobile.closeApplication()
	}

	@And("'Terlalu Banyak Percobaan' buttom sheet will appear")
	def verifyButtomSheet() {
		Mobile.tap(findTestObject('Object Repository/LoginPage/android.view.View'), 0)
	}

	@And("User performs (.*) consecutive failed login attempts with (.*) and (.*)")
	def inputConsecutiveFailedLoginAttempts(numAttempts, String username, String password) {
		inputEmailPassword(username, password)
		for (int i = 0; i < numAttempts; i++) {
			clickLoginPageLoginButton()
			Mobile.tap(findTestObject('Object Repository/LoginPage/android.widget.EditText - samuel.pangestuhotmail.com'), 0)
		}
	}
}

