
@tag
Feature: Title of your feature
Scenario Outline: Login with too many failed login request
    Given User is on the landing page
    When User clicks the login button on the landing page
    And User performs <numAttempts> consecutive failed login attempts with <username> and <password>
    Then 'Terlalu Banyak Percobaan' buttom sheet will appear

Examples:
    | numAttempts | username                   | password      |
    | 5            | samuel.pangestu@hotmail.com | wrongpassword |
    