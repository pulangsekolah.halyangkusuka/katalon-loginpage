Feature: Login Feature

Scenario Outline: Login with valid email and password
	Given User is on the landing page
	When User clicks the login button on the landing page
	And User inputs valid <username> and <password>
	And User clicks the login button on the login page
	Then User should be successfully logged in
	
    Examples:
    | username                      | password   |
    | samuel.pangestu@hotmail.com   | R4d!0G4ga  |
    
Scenario Outline: Login with valid email and invalid password
	Given User is on the landing page
	When User clicks the login button on the landing page
	And User inputs valid <username> and <password>
	And User clicks the login button on the login page
	Then Message 'Email atau password salah. Silakan coba lagi' will appear
		
		Examples:
    | username      								| password    	|
    | samuel.pangestu@hotmail.com   | wrongpassword |
    
Scenario Outline: Login with invalid email format
	Given User is on the landing page
	When User clicks the login button on the landing page
	And User inputs valid <username> and <password>
	And User clicks the login button on the login page
	Then Message 'Format email salah.' will appear

		Examples:
    | username      		| password    	|
    | samuel.pangestu   | wrongpassword |
    | #$%#$$%^$%    		| wrongpassword |
    
Scenario Outline: Login with blank email and password
	Given User is on the landing page
	When User clicks the login button on the landing page
	And User clicks the login button on the login page
	Then Message 'Wajib diisi.' will appear
	
Scenario Outline: Login with blank email and valid password
	Given User is on the landing page
	When User clicks the login button on the landing page
	And User inputs only valid <password>
	And User clicks the login button on the login page
	Then Message 'Wajib diisi.' will appear
	
		Examples:
    | password    |
    | R4d!0G4ga   |

Scenario Outline: Login with too many failed login request
    Given User is on the landing page
    When User clicks the login button on the landing page
    And User performs <numAttempts> consecutive failed login attempts with <username> and <password>
    Then 'Terlalu Banyak Percobaan' buttom sheet will appear

Examples:
    | numAttempts | username                   | password      |
    | 5            | samuel.pangestu@hotmail.com | wrongpassword |
    